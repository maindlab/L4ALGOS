﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace L4
{
    class Map
    {
        /// <summary>
        /// класс описывающий ребро
        /// </summary>
        class Edge
        {
            /// <summary>
            /// стартовая вершина ребра
            /// </summary>
            public int vertex1 { get; set; }
            /// <summary>
            /// коненая вершина ребра
            /// </summary>
            public int vertex2 { get; set; }
            /// <summary>
            /// макс пропусная способность ребра
            /// </summary>
            public int wight { get; set; }
            /// <summary>
            /// текущая пропусная способность ребра
            /// </summary>
            public int currentWight { get; set; } = 0;
            /// <summary>
            /// состояние ребра (можно ли из него двигаться)
            /// </summary>
            public bool stat { get; set; } = true;
            /// <summary>
            /// создание ребра
            /// </summary>
            /// <param name="vertex1">начальная вершина</param>
            /// <param name="vertex2">конечная вершина</param>
            /// <param name="wight">макс пропусная способность</param>
            public Edge(int vertex1,int vertex2,int wight)
            {
                if (vertex1 < 1 || vertex2 < 1 || wight < 0) throw new Exception("invalid data? when create edge");
                this.vertex1 = vertex1;
                this.vertex2 = vertex2;
                this.wight = wight;
            }
        }
        /// <summary>
        /// граф в виде списка ребер
        /// </summary>
        private List<Edge> map;
        private List<bool> vertexes;

        private Map()
        {
            map = new List<Edge> (){ new Edge(1,2,10),new Edge(2,3,5),
                                   new Edge(3,8,9),new Edge(8,5,9),
                                   new Edge(7,2,7),new Edge(4,7,7),
                                   new Edge(2,4,7),new Edge(5,3,9),
                                   new Edge(5,4,4),new Edge(2,6,5),
                                   new Edge(5,6,10)
                                  };
            vertexes = new List<bool>() { true, true, true, true, true, true, true, true, };
        }
        public static Map SetMyMap()
        {
            return new Map();
        } 
        /// <summary>
        /// прокдадывает возможный путь от истока к стоку
        /// </summary>
        /// <param name="startVert"> исток</param>
        /// <param name="endVert">сток </param>
        public bool SetPath(int startVert,int endVert, int count)
        {
            var list = new List<bool>();
            List<Edge> localMap = new List<Edge>();
            for (int i = 0; i < count; i++)
            {
                list.Add(true);
            }
            int minWight = -1;
            bool statOfPath = true;
            if (!vertexes[startVert - 1])
                return false;
            int startStepVert = startVert;
            minWight = -1;
            while (startStepVert != endVert && statOfPath)
            {
                
                bool statOfStep = true,  nextChek = true, IsAddEdge = false;
                list[startStepVert - 1] = false;
                var edges = GetVertWithBegin(startStepVert);
                if (edges.Count != 0)
                {
                    foreach (var edge in edges)
                    {
                        if (list[edge.vertex2 - 1] && edge.wight - edge.currentWight > 0)
                        {
                            startStepVert = edge.vertex2;
                            edge.stat = true;
                            localMap.Add(edge);
                            statOfStep = false;
                            if (minWight == -1 || edge.wight - edge.currentWight < minWight) {
                                if(minWight==-1)
                                    minWight = edge.wight - edge.currentWight;
                                if(minWight> edge.wight - edge.currentWight)
                                    minWight = edge.wight - edge.currentWight;
                            }
                              
                            IsAddEdge = true;
                            break;
                        }
                    }
                }
                if (edges.Count == 0 ||  !IsAddEdge)
                {
                    edges = GetVertWithEnd(startStepVert);
                    nextChek = false;
                }
                if (edges.Count != 0 && !nextChek)
                {
                    foreach (var edge in edges)
                    {
                        if (minWight != -1)
                        {
                            if (list[edge.vertex1 - 1] && edge.currentWight - minWight > 0)
                            {
                                startStepVert = edge.vertex1;
                                edge.stat = false;
                                localMap.Add(edge);
                                statOfStep = false;
                                IsAddEdge = true;
                                break;
                            }
                        }
                    }
                }
                if (!IsAddEdge)
                {
                    vertexes[startStepVert - 1] = false;
                    statOfPath = false;
                }
            }
                if(statOfPath)
                {
                    foreach (var edge in localMap)
                    {
                        if (edge.stat)
                            edge.currentWight += minWight;
                        else
                            edge.currentWight -= minWight;
                    }
                
            }
            return true;

        }
        /// <summary>
        /// возвращает список ребер текущего графа
        /// </summary>
        /// <param name="beginVert">начальная вершина</param>
        /// <returns></returns>
        private List<Edge> GetVertWithBegin(int beginVert)
        {
            var list = new List<Edge>();
            foreach(var edge in map)
            {
                if (edge.vertex1 == beginVert && vertexes[edge.vertex2-1]) list.Add(edge);
            }
            return list;
        }
        /// <summary>
        /// возвращает список ребер текущего графа
        /// </summary>
        /// <param name="endVert">конечная вершина</param>
        /// <returns></returns>
        private List<Edge> GetVertWithEnd(int endVert)
        {
            var list = new List<Edge>();
            foreach (var edge in map)
            {
                if (edge.vertex2 == endVert && vertexes[edge.vertex1-1]) list.Add(edge);
            }
            return list;
        }
        public List<int> GetSlice(int startVert)
        {
            List<int> slice = new List<int>(),arr = new List<int>();
            slice.Add(startVert);
            arr.Add(startVert);
            var edges = GetVertWithBegin(startVert);
            foreach(var edge in map)
            {
                if(edge.vertex1==startVert)
                if(edge.currentWight!=0 && edge.currentWight != edge.wight)
                {
                    slice.Add(edge.vertex2);
                }
            }
            return slice;
            
        }
    }
}
